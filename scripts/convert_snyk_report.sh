#!/bin/sh
alias convert_snyk_report="jq -r '([.[].infrastructureAsCodeIssues[].severity?]|flatten)|{\"low\":(map(select(.==\"low\"))|length),\"medium\":(map(select(.==\"medium\"))|length),\"high\":(map(select(.==\"high\"))|length),\"critical\":(map(select(.==\"critical\"))|length)}'"

cat snyk.json | convert_snyk_report