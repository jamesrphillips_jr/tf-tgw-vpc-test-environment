# Terraform Transit Gateway VPC Test Environment

## Description

This project creates a a multi-vpc transit gateway environment that can be used for the purposes of testing changes to the transit gateway or route creation, etc.

Basically anything that you need a multi-vpc environment attached to a transit gateway for.

## Resources
