variable "admin_sso_role" {
  type = string
}
variable "ci-user" {
  type = string
}
variable "costcenter" {
  type = string
}
variable "organization" {
  type = string
}
variable "environment" {
  type = string
}
