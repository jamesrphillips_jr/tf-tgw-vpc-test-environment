terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>3.50.0"
    }
  }
}

provider "aws" {
  default_tags {
    tags = {
      Environment = var.environment
      CostCenter  = var.costcenter
    }
  }
  region = var.region
}
