module "tgw" {
  source = "terraform-aws-modules/transit-gateway/aws"

  name        = "fncz-${data.aws_region.current.name}-tgw"
  description = "test tgw from hashi terraform tgw module"

  enable_auto_accept_shared_attachments = true

  vpc_attachments = {
    vpc = {
      vpc_id      = module.spoke_a.vpc_id
      subnet_ids  = module.spoke_a.private_subnets
      dns_support = true
    }
  }

  ram_name       = "${data.aws_region.current.name}-tgw"
  ram_principals = [data.aws_organizations_organization.current.arn]

  ram_tags = {
    Foo = "Bar"
  }

  tags = {
    Alice = "Bob"
  }
}
module "spoke_a" {
  source = "github.com/fortunecookiezen/terraform-aws-fncz-spoke-vpc"
  name   = "spoke-a"
  cidr   = "10.10.0.0/16"

  private_subnets = ["10.10.4.0/24", "10.10.5.0/24"]

  attach_transit_gateway = false
  # transit_gateway_id     = module.tgw.ec2_transit_gateway_id
  # transit_gateway_routes = {} # if you don't want to declare tgw routes
  transit_gateway_routes = {
    default = {
      destination_cidr_block = "0.0.0.0/0"
      transit_gateway_id     = module.tgw.ec2_transit_gateway_id
    }
  }

  tags = {
    Owner       = "spoke-a"
    Environment = "shared"
  }
}

# module "spoke_b" {
#   source = "github.com/fortunecookiezen/terraform-aws-fncz-spoke-vpc"
#   name   = "spoke-b"
#   cidr   = "10.20.0.0/16"

#   private_subnets = ["10.20.4.0/24", "10.20.5.0/24"]

#   attach_transit_gateway = true
#   transit_gateway_id     = aws_ec2_transit_gateway.transit_gateway.id


#   transit_gateway_routes = {
#     default = {
#       destination_cidr_block = "0.0.0.0/0"
#       transit_gateway_id     = aws_ec2_transit_gateway.transit_gateway.id
#     }
#   }

#   tags = {
#     Owner       = "spoke-b"
#     Environment = "prod"
#   }
# }

# module "spoke_c" {
#   source = "github.com/fortunecookiezen/terraform-aws-fncz-spoke-vpc"
#   name   = "spoke-c"
#   cidr   = "10.30.0.0/16"

#   private_subnets = ["10.30.4.0/24", "10.30.5.0/24"]

#   attach_transit_gateway = true
#   transit_gateway_id     = aws_ec2_transit_gateway.transit_gateway.id

#   transit_gateway_routes = {
#     default = {
#       destination_cidr_block = "0.0.0.0/0"
#       transit_gateway_id     = aws_ec2_transit_gateway.transit_gateway.id
#     }
#   }

#   tags = {
#     Owner       = "spoke-c"
#     Environment = "nonprod"
#   }
# }

# resource "aws_ec2_transit_gateway_route_table" "nonprod" {
#   transit_gateway_id = aws_ec2_transit_gateway.transit_gateway.id
#   tags = {
#     "Name" = "nonprod-tgw-rt"
#     "Zone" = "nonprod"
#   }
# }

# resource "aws_ec2_transit_gateway_route_table" "shared" {
#   transit_gateway_id = aws_ec2_transit_gateway.transit_gateway.id
#   tags = {
#     "Name" = "shared-tgw-rt"
#     "Zone" = "shared"
#   }
# }

# resource "aws_ec2_transit_gateway_route_table" "prod" {
#   transit_gateway_id = aws_ec2_transit_gateway.transit_gateway.id
#   tags = {
#     "Name" = "prod-tgw-rt"
#     "Zone" = "production"
#   }
# }
