variable "region" {
  type = string
}
variable "costcenter" {
  type = string
}
variable "environment" {
  type = string
}
variable "organization" {
  type = string
}
